﻿
namespace DesignPatterns.Prototype
{
    public class Line
    {
        public Point Start { get; set; }
        public Point End { get; set; }

        public Line DeepCopy()
        {
            var l = new Line();
            l.Start = Start.DeepCopy();
            l.End = End.DeepCopy();
            return l;
        }
    }
}
