﻿namespace DesignPatterns.Prototype
{
    public class Point : IPrototype<Point>
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point DeepCopy()
        {
            return new Point(X, Y);
        }
    }
}
