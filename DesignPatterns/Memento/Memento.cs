﻿using System.Collections.Generic;

namespace DesignPatterns.Memento
{
    public class Memento
    {
        public List<Token> Tokens = new List<Token>();
    }
}
