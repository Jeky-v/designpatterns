﻿using System.Collections.Generic;

namespace DesignPatterns.Composite
{
    public interface IValueContainer : IEnumerable<int>
    {
    }
}
