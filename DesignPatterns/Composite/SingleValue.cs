﻿using System.Collections;
using System.Collections.Generic;

namespace DesignPatterns.Composite
{
    public class SingleValue : IValueContainer
    {
        public int value;

        public IEnumerator<int> GetEnumerator()
        {
            yield return value; 
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new System.NotImplementedException();
        }
    }
}
