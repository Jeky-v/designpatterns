﻿using System;

namespace DesignPatterns.ChainOfResponsibility
{
    public class Goblin : Creature
    {
        public override int Attack
        {
            get
            {
                var q = new StatQuery { Statistics = Statistics.Attack };
                foreach (var c in game.Creatures)
                {
                    c.Query(this, q);
                }
                return q.Value;
            }
        }

        public override int Defense
        {
            get
            {
                var q = new StatQuery { Statistics = Statistics.Defense };
                foreach (var c in game.Creatures)
                {
                    c.Query(this, q);
                }
                return q.Value;
            }
        }

        public override void Query(object source, StatQuery sq)
        {
            if (ReferenceEquals(source, this))
            {
                switch (sq.Statistics)
                {
                    case Statistics.Attack:
                        sq.Value += baseAttack;
                        break;
                    case Statistics.Defense:
                        sq.Value += baseDefense;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else
            {
                if (sq.Statistics == Statistics.Defense)
                {
                    sq.Value++;
                }
            }
        }

        public Goblin(Game game) : base(game, 1, 1)
        {
        }

        protected Goblin(Game game, int baseAttack, int baseDefense) : base(game,
          baseAttack, baseDefense)
        {
        }
    }
}
