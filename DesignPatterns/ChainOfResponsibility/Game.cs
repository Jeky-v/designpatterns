﻿using System.Collections.Generic;

namespace DesignPatterns.ChainOfResponsibility
{
    public class Game
    {
        public IList<Creature> Creatures { get; set; } = new List<Creature>();
    }
}
