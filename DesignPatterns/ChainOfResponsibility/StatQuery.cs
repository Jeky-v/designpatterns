﻿namespace DesignPatterns.ChainOfResponsibility
{
    public class StatQuery
    {
        public Statistics Statistics { get; set; }
        public int Value { get; set; }
    }
}
