﻿namespace DesignPatterns.ChainOfResponsibility
{
    public enum Statistics
    {
        Attack,
        Defense
    }
}
