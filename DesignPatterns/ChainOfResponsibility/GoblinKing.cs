﻿namespace DesignPatterns.ChainOfResponsibility
{
    public class GoblinKing : Goblin
    {
        public GoblinKing(Game game) : base(game, 3, 3)
        {
        }

        public override void Query(object source, StatQuery sq)
        {
            if (!ReferenceEquals(source, this) && sq.Statistics == Statistics.Attack)
            {
                sq.Value++; // every goblin gets +1 attack
            }
            else base.Query(source, sq);
        }
    }
}
