﻿namespace DesignPatterns.ChainOfResponsibility
{
    public abstract class Creature
    {
        protected Game game;
        protected int baseAttack;
        protected int baseDefense;

        public virtual int Attack { get; set; }
        public virtual int Defense { get; set; }

        public Creature(Game g, int a, int d)
        {
            game = g;
            baseAttack = a;
            baseDefense = d;
        }

        public abstract void Query(object source, StatQuery sq);
    }
}
