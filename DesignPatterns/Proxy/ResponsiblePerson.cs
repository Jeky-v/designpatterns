﻿namespace DesignPatterns.Proxy
{
    class ResponsiblePerson
    {
        private Person _person;

        public ResponsiblePerson(Person person)
        {
            _person = person;
        }

        public string Drink()
        {
            if(_person.Age < 18)
            {
                return "too young";
            }
            return _person.Drink();
        }

        public string Drive()
        {
            if(_person.Age < 16)
            {
                return "too young";
            }
            return _person.Drive();
        }

        public string DrinkAndDrive()
        {
            return "dead";
        }
    }
}
