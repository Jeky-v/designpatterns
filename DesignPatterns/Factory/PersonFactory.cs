﻿namespace DesignPatterns.Factory
{
    public class PersonFactory
    {
        private int personId = -1;
        public Person CreatePerson(string name)
        {
            personId++;
            return new Person() { Id = personId, Name = name };
        }
    }
}
