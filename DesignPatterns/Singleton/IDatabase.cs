﻿namespace DesignPatterns.Singleton
{
    public interface IDatabase
    {
        int GetPopulation();
    }
}
