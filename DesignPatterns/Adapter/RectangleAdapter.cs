﻿namespace DesignPatterns.Adapter
{
    public class RectangleAdapter : IRectangle
    {
        private Square rect;

        public RectangleAdapter(Square square)
        {
            rect = square;
        }

        public int Width => rect.Side;
            
        public int Height => rect.Side;
    }
}
