﻿namespace DesignPatterns.Adapter
{
    interface IRectangle
    {
        int Width { get; }
        int Height { get; }
    }
}
