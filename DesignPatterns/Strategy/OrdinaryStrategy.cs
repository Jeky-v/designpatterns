﻿namespace DesignPatterns.Strategy
{
    public class OrdinaryDiscriminantStrategy : IDiscriminantStrategy
    {
        public double CalculateDiscriminant(double a, double b, double c)
        {
            var d = b * b - 4 * a * c;
            return d >= 0 ? d : double.NaN;
        }
    }
}
