﻿namespace DesignPatterns.Mediator
{
    public class Participant
    {
        private Mediator mediator;

        public int Value { get; set; }

        public Participant(Mediator mediator)
        {
            this.mediator = mediator;
            mediator.Participants.Add(this);
        }

        public void Say(int n)
        {
            mediator.Broadcast(this, n);
        }

        public void Receive(int value)
        {
            Value += value;
        }
    }
}
