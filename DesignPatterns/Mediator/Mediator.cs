﻿using System.Collections.Generic;

namespace DesignPatterns.Mediator
{
    public class Mediator
    {
        public List<Participant> Participants = new List<Participant>();

        public void Broadcast(Participant sender, int value)
        {
            foreach (var item in Participants)
            {
                if (item != sender)
                {
                    item.Receive(value);
                }
            }
        }
    }
}
