﻿using System;

namespace DesignPatterns.Observer
{
    public class Game
    {
        public event EventHandler RatAdded;
        public event EventHandler RatRemoved;
        public event EventHandler<Rat> NotifyRat;

        public void AddRat(object sender)
        {
            RatAdded?.Invoke(sender, EventArgs.Empty);
        }

        public void RemoveRat(object sender)
        {
            RatRemoved?.Invoke(sender, EventArgs.Empty);
        }

        public void NotifyRats(object sender, Rat whichRat)
        {
            NotifyRat?.Invoke(sender, whichRat);
        }
    }
}
