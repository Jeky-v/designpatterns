﻿using System;

namespace DesignPatterns.Observer
{
    public class Rat : IDisposable
    {
        public int Attack = 1;
        public Game game;

        public Rat(Game game)
        {
            this.game = game;
            game.RatAdded += RatAdded;
            game.RatRemoved += RatRemoved;
            game.NotifyRat += NotifyRat;
            game.AddRat(this);
        }

        public void NotifyRat(object sender, Rat args)
        {
            if (args == this) Attack++;
        }

        public void RatAdded(object sender, EventArgs args)
        {
            if(sender != this)
            {
                Attack++;
                game.NotifyRats(this, (Rat)sender);
            }
        }

        public void RatRemoved(object sender, EventArgs args)
        {
            Attack--;
        }

        public void Dispose()
        {
            game.RatAdded -= RatAdded;
            game.RatRemoved -= RatRemoved;
            game.NotifyRat -= NotifyRat;
            game.RemoveRat(this);
        }
    }
}
