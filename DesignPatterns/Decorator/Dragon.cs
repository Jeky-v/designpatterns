﻿namespace DesignPatterns.Decorator
{
    public class Dragon
    {
        private int _age;
        private Bird _bird;
        private Lizard _lizard;

        public Dragon()
        {
            _bird = new Bird();
            _lizard = new Lizard();
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            { 
                _age = value;
                _bird.Age = value;
                _lizard.Age = value;
            }
        }

        public string Fly()
        {
            return _bird.Fly();
        }

        public string Crawl()
        {
            return _lizard.Crawl();
        }
    }
}
