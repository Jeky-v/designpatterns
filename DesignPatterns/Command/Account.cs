﻿namespace DesignPatterns.Command
{
    public class Account
    {
        public int Balance { get; set; }

        public void Process(Command c)
        {
            switch (c.Action)
            {
                case Action.Deposit:
                    {
                        c.Success = true;
                        Balance += c.Amount; 
                        break;
                    }
                case Action.Withdraw:
                    {
                        if(Balance < c.Amount)
                        {
                            c.Success = false;
                        }
                        else
                        {
                            Balance -= c.Amount;
                            c.Success = true;
                        }
                        break;
                    }
            }
        }
    }
}
