﻿namespace DesignPatterns.Bridge
{
    public class Triangle : Shape
    {
        public Triangle(IRenderingStrategy renderer) : base(renderer)
        {
            Name = "Triangle";
        }

        public override string ToString()
        {
            return $"Drawing {Name} as {_renderer.WhatToRenderAs}";
        }
    }
}
