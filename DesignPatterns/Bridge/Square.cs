﻿namespace DesignPatterns.Bridge
{
    public class Square : Shape
    {
        public Square(IRenderingStrategy renderer) : base(renderer)
        {
            Name = "Square";
        }

        public override string ToString()
        {
            return $"Drawing {Name} as {_renderer.WhatToRenderAs}";
        }
    }
}
