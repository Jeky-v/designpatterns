﻿namespace DesignPatterns.Bridge
{
    public interface IRenderingStrategy
    {
        string WhatToRenderAs { get; }
    }
}
