﻿namespace DesignPatterns.Bridge
{
    public class RasterRenderer : IRenderingStrategy
    {
        public string WhatToRenderAs => "pixels";
    }
}
