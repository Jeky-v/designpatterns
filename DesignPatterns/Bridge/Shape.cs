﻿namespace DesignPatterns.Bridge
{
    public abstract class Shape
    {
        protected IRenderingStrategy _renderer;

        public string Name { get; set; }

        public Shape(IRenderingStrategy renderer)
        {
            _renderer = renderer;
        }
    }
}
