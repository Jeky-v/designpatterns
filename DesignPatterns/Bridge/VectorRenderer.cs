﻿namespace DesignPatterns.Bridge
{
    public class VectorRenderer : IRenderingStrategy
    {
        public string WhatToRenderAs => "lines";
    }
}
