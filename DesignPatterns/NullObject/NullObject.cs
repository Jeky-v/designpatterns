﻿namespace DesignPatterns.NullObject
{
    public class NullObject : ILog
    {
        public int RecordLimit => int.MaxValue;

        public int RecordCount { get; set; } = int.MinValue;

        public void LogInfo(string message)
        {
            ++RecordCount;
        }
    }
}
