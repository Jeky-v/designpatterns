﻿using System;

namespace DesignPatterns.Builder
{
    public class Property
    {
        public AccessModifier Modifier { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }

        public Property(string name, string type, AccessModifier modifier = AccessModifier.Public)
        {
            Modifier = modifier;
            Name = name;
            Type = type;
        }

        public override string ToString()
        {
            return $"{Modifier.ToString().ToLowerInvariant()} {Type} {Name};";
        }
    }
}
