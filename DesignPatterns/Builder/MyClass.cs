﻿using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Builder
{
    public class MyClass
    {
        private int _intedation;

        public AccessModifier Modifier { get; set; }
        public string Name { get; set; }
        public List<Property> Properties {get; set;}

        public MyClass(string name, AccessModifier modifier, int intedation)
        {
            Name = name;
            Modifier = modifier;
            Properties = new List<Property>();
            _intedation = intedation;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append($"{Modifier.ToString().ToLowerInvariant()} class {Name}").AppendLine();
            sb.Append("{").AppendLine();
            foreach (var prop in Properties)
            {
                sb.Append(' ', _intedation).Append(prop).AppendLine();
            }
            sb.Append("}");

            return sb.ToString();
        }
    }
}
