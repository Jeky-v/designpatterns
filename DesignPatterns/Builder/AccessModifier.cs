﻿
namespace DesignPatterns.Builder
{
    public enum AccessModifier
    {
        Public,
        Private,
        Protected,
        Internal
    }
}
