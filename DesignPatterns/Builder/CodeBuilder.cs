﻿namespace DesignPatterns.Builder
{
    public class CodeBuilder
    {
        private MyClass _code;

        public CodeBuilder(string className, AccessModifier modifier = AccessModifier.Public)
        {
            _code = new MyClass(className, modifier, 2);
        }

        public CodeBuilder AddField(string name, string type, AccessModifier modifier = AccessModifier.Public)
        {
            _code.Properties.Add(new Property(name, type, modifier));
            return this;
        }

        public override string ToString()
        {
            return _code.ToString();
        }
    }
}
