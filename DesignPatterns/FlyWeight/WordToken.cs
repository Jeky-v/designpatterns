﻿namespace DesignPatterns.FlyWeight
{
    public class WordToken
    {
        public bool Capitalize { get; set; }
    }
}
