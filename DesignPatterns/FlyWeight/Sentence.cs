﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.FlyWeight
{
    public class Sentence
    {
        private List<string> _words;
        private Dictionary<int, WordToken> _tokens = new Dictionary<int, WordToken>();

        public Sentence(string plainText)
        {
            _words = plainText.Split(' ').ToList();
        }

        public WordToken this[int index]
        {
            get
            {
                if(index < _words.Count)
                {
                    if (_tokens.ContainsKey(index))
                    {
                        return _tokens[index];
                    }

                    var t = new WordToken();
                    _tokens.Add(index, t);
                    return t;
                }

                return null;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < _words.Count; i++)
            {
                if (_tokens.ContainsKey(i))
                {
                    var isCap = _tokens[i].Capitalize;
                    sb.Append(isCap ? _words[i].ToUpper() : _words[i]);
                }
                else
                {
                    sb.Append(_words[i]);
                }
                sb.Append(' ');
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }
    }
}
